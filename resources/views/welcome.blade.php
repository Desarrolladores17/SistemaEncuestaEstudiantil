<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .detalles{
                text-align: match-parent;
                text-justify: auto;
            }
            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Sistema de Encuesta Estudiantil
                </div>
                <div class="detalles">
                    <ul>
                        <li>Ingresa a las encuestas haciendo click sobre su icono y califica el desempeño de tu docente</li>
                        <li>Recuerda que debes completarlas todas para poder finalizar e imprimir tu constancia de participación</li>
                        <li>Si no puedes completarlas todas ahora, tu progreso quedará guardado automáticamente.</li>
                    </ul>
                </div>
                <div class="links">
                    <a href="https://laravel.com/docs">Algoritmica I</a>
                    <a href="https://laracasts.com">Calculo I</a>
                    <a href="https://laravel-news.com">Matematica Basica I</a>
                    <a href="https://forge.laravel.com">Teoria de Sistemas</a>
                    <a href="https://github.com/laravel/laravel">Computación e Informática</a>
                </div>
            </div>
        </div>
    </body>
</html>
